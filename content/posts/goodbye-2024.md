---
title: "Bidding 2024 a Goodbye"
categories: ["personal"]
tags: ["introspection", "thoughts"]
description: "Acknowledging all the luck and lessons."
summary: "Acknowledging all the luck and lessons."
date: "2024-12-31"
ShowToc: false
---

"Growing up in itself is hard." - A good friend, Sharon, consoled me when I was messed up. It made me ponder over how my life has changed over the past few years - how much and how well, or worse, have I grown.
Graduating and moving to a faraway city for a job - this was the biggest turn. Living on my own was a true test of myself - a real introspection on my personality, (in)capabilities, priorities, ideologies, and more. Learning that I'm over-aged while applying for government exams made me realize that I've moved to a new phase of life, running out of over 33% of my (hopefully long) life. I don't yet know what it is, but it has cast a shadow of adulting anxiety on me.

## 2024 - The best I could've asked for
If life is a school, 2024 was my "Life-skills" class. One thing it taught me - keep moving forward; even if you're in darkness, you just need to take your next one step. There will be light at the end of the tunnel. And some pursuits - you have to take them alone, because you're destined for it.

## Life takes turns
Running a race of 10 lakh people, and trying to not be the rat in it, was yet another turn I took in 2024. This has been yet another test - cognitive, emotional, and physical.

Relationships with people have maintained an equilibrium - lost touch with some and got some new connections as I had to catch up with my current race.

## What 2025 comes with
2025 is gonna be more decisive in the course - more adventures and even more lessons.
My only new year resolution is to post more blogs here (Something that I've been *consistently procrastinating* since the past four years!).

Cheers to 2025 - hoping for a better world!
